#!flask/bin/python
# -- coding: utf-8 --

__author__ = 'cloudtogo'

from flask import Flask
from flask import request
import time

app = Flask(__name__)

"""
接口和方法说明：
get      /  检查服务是否正常启动
get      /users  获取用户列表
options  /users  清空用户

post     /user   添加用户   application/json   {"userName": "your name", "phone": "your phone"}
get      /user/<userId>  获取用户详情    
put      /user/<userId>  修改用户信息   application/json   {"userName": "your name", "phone": "your phone"}
delete   /user/<userId>  删除当前用户
"""

users = []

@app.route('/')
def hello():
    return "it works111"

# get users
@app.route('/users', methods=['GET'])
def get_users():
    return {"code": 0, "users": users}

# get user
@app.route('/users/<userId>', methods=['GET'])
def get_user(userId):
    for user in users:
        if user['userId'] == userId:
            return {"code": 0, "user": user}
    return {"code": 0, "user": {}}

# add user
# {"userName": "userName", "phone": "phone"}
@app.route('/user', methods=['POST'])
def add_user():
    data = request.json
    user_id = str(time.time())
    users.append({"userId": user_id, "user": data})
    return {"code": 0, "userId": user_id}

# 修改 user
# {"userName": "userName", "phone": "phone"}
@app.route('/user/<userId>', methods=['PUT'])
def update_user(userId):
    data = request.json
    for user in users:
        if userId == user.get("userId"):
            user['user'] = data
            return {"code": 0}
    return {"code": -1, "message": "user not found"}

# delete user
@app.route('/user/<userId>', methods=['DELETE'])
def delete_user(userId):
    for user in users:
        if userId == user.get("userId"):
            users.pop(user)
            return {"code": 0}
    return {"code": -1, "message": "user not found"}

# clear user
@app.route('/users', methods=['OPTIONS'])
def clear_users():
    users.clear()
    return {"code": 0}


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
